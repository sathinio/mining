#!/bin/bash

#Enable Fan Control
#nvidia-settings -a '[gpu:0]/GPUFanControlState=1'

#Specify Fan Speed
#nvidia-settings -a '[fan:0]/GPUTargetFanSpeed=50'

#OverClock GPU
nvidia-settings -a [gpu:0]/GPUGraphicsClockOffsetAllPerformanceLevels=180
nvidia-settings -a [gpu:1]/GPUGraphicsClockOffsetAllPerformanceLevels=180
nvidia-settings -a [gpu:2]/GPUGraphicsClockOffsetAllPerformanceLevels=180
nvidia-settings -a [gpu:3]/GPUGraphicsClockOffsetAllPerformanceLevels=180
nvidia-settings -a [gpu:4]/GPUGraphicsClockOffsetAllPerformanceLevels=180
nvidia-settings -a [gpu:5]/GPUGraphicsClockOffsetAllPerformanceLevels=180

#OverClock Memory
nvidia-settings -a [gpu:0]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800 
nvidia-settings -a [gpu:1]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800 
nvidia-settings -a [gpu:2]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800 
nvidia-settings -a [gpu:3]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800
nvidia-settings -a [gpu:4]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800 
nvidia-settings -a [gpu:5]/GPUMemoryTransferRateOffsetAllPerformanceLevels=1800 

#Power Limit Enable
nvidia-smi -i 0 --persistence-mode=1
nvidia-smi -i 1 --persistence-mode=1
nvidia-smi -i 2 --persistence-mode=1
nvidia-smi -i 3 --persistence-mode=1
nvidia-smi -i 4 --persistence-mode=1
nvidia-smi -i 5 --persistence-mode=1

#Limit Power
nvidia-smi -i 0 --power-limit=80
nvidia-smi -i 1 --power-limit=80
nvidia-smi -i 2 --power-limit=80
nvidia-smi -i 3 --power-limit=80
nvidia-smi -i 4 --power-limit=80
nvidia-smi -i 5 --power-limit=80

#./ethminer -U -P stratum://sp_stam.1234@eth-us.sparkpool.com:3333
#./ethminer -U -P stratum://0x3129B3AdA1B4DA3b7F3832B1B7E52b179E2e0DD1.Nodas@us1.ethermine.org:4444