FROM nvidia/cuda:11.2.2-runtime-ubuntu20.04

MAINTAINER Stam Athiniotis

WORKDIR /

ENV DEBIAN_FRONTEND=noninteractive

# Package and dependency setup
RUN apt-get update \
	&& apt-get install -y git cmake build-essential software-properties-common wget \
	&& wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin \
	&& mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600 \
	&& apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub \
	&& add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /" \
	&& apt-get update \
	&& apt-get -y install cuda \
	&& apt-get clean && rm -rf /var/lib/apt/lists/*

# Build. Use all cores.
RUN git clone https://github.com/ethereum-mining/ethminer.git \
	&& cd ethminer \
	&& git submodule update --init --recursive \
	&& mkdir build \
	&& cd build \
	&& cmake .. -DETHASHCL=OFF -DETHASHCUDA=ON -DAPICORE=ON -DBINKERN=OFF -DETHDBUS=OFF -DUSE_SYS_OPENCL=OFF \
	&& cmake --build . -- -j \
	&& make install